<?php

namespace Drupal\form_mode_routing\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes for form modes.
 */
class NewRoutes {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a NewRoutes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    /** @var \Drupal\form_mode_routing\Entity\FormRoutingEntityInterface[] $modes */
    $modes = $this->entityTypeManager->getStorage('form_routing_entity')
      ->loadMultiple();

    // Create new if dont exist.
    $routes = [];

    foreach ($modes as $mode) {
      $form_mode = $mode->label();
      $entity_details = explode('.', $form_mode);

      $entity_type = $entity_details[0];
      if ($entity_type == 'group') {
        // Left to debug your entity type.
        // dump(\Drupal::entityTypeManager()->getDefinition($entity_type));
      }

      $title = 'Edit';
      if (!empty($mode->title)) {
        $title = $mode->title;
      }
      $route_name = 'form_mode_routing.' . $form_mode;

      // Create.
      $routes[$route_name] = new Route(
        // Path to attach this route to:
        $mode->path,
        // Route defaults:
        [
          '_entity_form' => $form_mode,
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
        ],
        // Route requirements:
        [
          '_entity_access'  => $entity_type . ".update",
          '_custom_access'  => '\Drupal\form_mode_routing\Access\CustomAccessCheck::access',
        ],
        // Route Options.
        [
          '_admin_route' => $mode->admin
        ],
        // Route host.
        '',
        [],
        [
          'GET',
          'POST',
        ]
      );
    }
    return $routes;

  }

}
